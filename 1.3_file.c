//�������� ���������, ������� �������� �� ���� �������� ���� � ���������� �� �� 
//� ��������� ������� �� ��� ��� �����������. ����������� ����������� � ������ ���� � ��������� ������� �����, 
//��� ��� �����������. ����������: ����������� ����� ���� ������������� � ��������������.
/*
1. ������� ������
2. ��������� ������ �� �����
3. ������ ������ � "//" �� "\n" ��� � / �� \
4. ������� �����
5. ��������� ��������� ������
*/

#include <stdio.h>
#include <conio.h>
#include <string.h>

int main ()
{
	int count=1, i=0; //count ��� �������� �����
	int flag=0, flag_c=0; // flag - ���� �� ����������� '//', flag_c - ���� �� ����������� ������ '/*...*/'
	int ch, old=0, oldold=0, prev=0; // ch - ������ ����������� �� ����� ������, old - ������ �� ���, oldold - ������ �� old
		
	FILE *fp, *fout, *fmod; // ��������� �� �������� ���� � �����, �� �������� ���� � ���������� ��� ���������, �� �������� ���� � ����������
	fp=fopen("in.txt","r");
	fout=fopen("out.txt","w");
	fmod=fopen("mod.txt","w");

	if(fp==0)
	{
		puts("Error!");
		return 1;//exit
	}
	
	while((ch=fgetc(fp))!=EOF)
	{
		if (flag_c==1 || flag==1) // �� ����� ��������� ������ ����������� ����� �����
		{
			putc(ch, fout);
		}
		
		if (flag==0 || flag_c==0) // �� ����� �� ������ �����������
		{
			putc(oldold, fmod);
		}

		if (old=='*' && oldold=='/')
		{
			flag_c=1; // ���� �� ����������� ���� 2
			fprintf(fout,"comment #%d ", count);
		}

		if (ch=='/' && old=='/' && flag_c==0)
		{
			flag=1; // ���� �� ����������� ���� 1
			fprintf(fout, "comment #%d ", count);
		}
		
		if (ch=='\n')// � ����� ������ ������� ������ � �������� ���� ���� 1
		{
			count++;
			flag=0;
		}
		if (ch=='/' && old=='*')//�������� ���� ���� 2
		{
			flag_c=0;
		}

		oldold=old;
		old=ch;
	}
	
	fclose(fp);
	fclose(fout);

	printf("%d", count);
	
	return 0;
}