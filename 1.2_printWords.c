//�������� ���������, ������� �������� �� ���� �������� ���� � ���������� �� �� 
//� ��������� ������� �� ��� ��� �����������. ����������� ����������� � ������ ���� � ��������� ������� �����, 
//��� ��� �����������. ����������: ����������� ����� ���� ������������� � ��������������.
/*
1. ������� ������
2. ��������� ������ �� �����
3. ������ ������ � "//" �� "\n" ��� � / �� \
4. ������� �����
5. ��������� ��������� ������
*/

#include <stdio.h>
#include <conio.h>
#include <string.h>

int main ()
{
	int countString[]={0},k=0, count=1, i=0;
	int inComment=0, flag=0;
	int ch, old=0, oldold=0;
		
	FILE *fp, *fout, *fmod;
	fp=fopen("in.txt","r");
	fout=fopen("out.txt","w");
	fmod=fopen("mod.txt","w");

	if(fp==0)
	{
		puts("Error!");
		return 1;//exit
	}
	
	while((ch=fgetc(fp))!=EOF)
	{
		if (flag==1)
		{
			putc(ch, fout);
		}

		if (flag==0)
		{
			putc(oldold, fmod);
		}

		if (ch=='/' && old=='/')
		{
			flag=1;
			fprintf(fout, "comment #%d ", count);
		}
		
		if (ch=='\n')
		{
			count++;
			flag=0;
		}
		oldold=old;
		old=ch;
	}
	
	fclose(fp);
	fclose(fout);

	printf("%d", count);
	
	return 0;
}