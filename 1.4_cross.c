#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<math.h>
#define N 28

char mass[N][N];
void cross (int, int, int);
void frac (int x, int y, int max)
{
	int x1,x2,x3,x4,x5, y1,y2,y3,y4,y5, l1;
	l1=(int)pow(3,max);
	cross(x,y,l1);
	l1=(int)pow(3,max-1);
	 	
	x1=x;y1=y-l1;
	x2=x+l1;y2=y;
	x3=x;y3=y+l1;
	x4=x-l1;y4=y;
	x5=x;y5=y;
	
	if (max>1)
	{
		frac(x1,y1,max-1);
		frac(x2,y2,max-1);
		frac(x3,y3,max-1);
		frac(x4,y4,max-1);
		frac(x5,y5,max-1);
	}
}
		

int main ()
{
    int i,j;
	for (i = 0; i  < N ; i++)
	{
		for (j = 0; j < N-1; j++)
		{
			mass[i][j]=' ';
		}
	mass[i][N-1]=0;
	}

  frac(N/2,(N-1)/2,3);
  
  for (i = 0; i  < N ; i++)
	{ 
	 for (j = 0; j < N-1; j++)
         {
          printf(" ");
          putchar(mass[i][j]);
         }
	 
	 printf("\n");
	}
	printf("\n");
	printf("\n");
	
	return 0;

}

void cross(int x,int y,int l)
{ 
  int i,j;
  for (i=x-l/2; i<=x+l/2; i++)
    mass[i][y]='*';
	for (j=y-l/2; j<=y+l/2; j++)
	   mass[x][j]='*';
 }
